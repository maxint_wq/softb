

export default {
  post: query => {
    return fetch('http://192.168.1.145:8011/api/query/do', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        query: query,
      }),
    }).then((response) => response.json());
  },
};
