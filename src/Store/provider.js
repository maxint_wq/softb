import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {Provider} from 'react-redux';
import createStore from './index';

import * as eva from '@eva-design/eva';
import {ApplicationProvider, IconRegistry, Layout, Text} from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';

let store;

class AppStoreProvider extends PureComponent {
  render() {
    const {children} = this.props;

    store = store || createStore;
    return (
      <>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider {...eva} theme={eva.light}>
        <Provider store={createStore.store}>{children}</Provider>
      </ApplicationProvider>
      </>
    );
  }
}

export default AppStoreProvider;
