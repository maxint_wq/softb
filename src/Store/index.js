import AsyncStorage from '@react-native-community/async-storage';
import {createStore, applyMiddleware} from 'redux';
import {createLogger} from 'redux-logger';
import {persistStore, persistReducer} from 'redux-persist';
import rootReducer from '../Reducers/index';
import thunkMiddleware from 'redux-thunk'
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['User'],
  blacklist: [],
};
const persistedReducer = persistReducer(persistConfig, rootReducer);
const store = createStore(persistedReducer, applyMiddleware(thunkMiddleware, createLogger()));
let persistor = persistStore(store);
export default {store, persistor};
