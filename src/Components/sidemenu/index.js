/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  View,
  Text,
  StatusBar,
} from 'react-native';
import {Button, Icon, List, ListItem, withStyles} from '@ui-kitten/components';
import {AvatarComponent} from '../../FunctionalComponents';
import {Navigation} from 'react-native-navigation';

import {HOME_SCREEN, PROFILE_SCREEN} from '../../Navigation/Screens';

import {connect} from 'react-redux';

const SideMenu: () => React$Node = props => {
  const {eva, style, User, dispatch, ...restProps} = props;

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <View style={styles.body}>
          <View style={styles.sectionContainer}>
            <AvatarComponent
              user={User.userprofile}
              onClickImage={() => {
                //console.log('onClickImage', props.componentId, HOME_SCREEN);
                Navigation.mergeOptions('bottom_tab_id', {
                  bottomTabs: {
                    currentTabId: `${HOME_SCREEN}_id`,
                  },
                });
                Navigation.push(`${HOME_SCREEN}_id`, {
                  component: {
                    name: PROFILE_SCREEN,
                    options: {
                      sideMenu: {
                        left: {
                          visible: false,
                        },
                      },
                    },
                  },
                });
              }}
              onClickInfo={() => {}}
            />
          </View>
          <List
            style={styles.listContainer}
            data={[
              {
                title: 'Action1',
                iconleft: props => (
                  <Icon {...props} name="shield-off-outline" />
                ),
              },
              {
                title: 'Action2',
                iconleft: props => <Icon {...props} name="sun-outline" />,
              },
              {
                title: 'Logout',
                iconleft: props => <Icon {...props} name="log-out-outline" />,
              },
            ]}
            renderItem={({item, index}) => {
              return (
                <ListItem
                  title={evaProps => <Text {...evaProps}>{item.title}</Text>}
                  accessoryRight={item.iconleft}
                />
              );
            }}
          />
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  body: {
    backgroundColor: 'white',
    height: '100%',
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  listContainer: {},
});

export const ThemedView = withStyles(SideMenu, theme => ({
  iconcolor: {
    backgroundColor: theme['color-basic-900'],
  },
}));

const mapStateToProps = state => ({
  User: state.User,
});

ThemedView.defaultProps = {
  User: {
    userprofile: {
      id: '',
      profileimage: '',
      userid: '',
      userpassword: '',
    },
  },
};

export default connect(mapStateToProps)(ThemedView);
