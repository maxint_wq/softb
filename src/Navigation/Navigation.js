// @flow

import {Navigation} from 'react-native-navigation';
import Icon from 'react-native-vector-icons/MaterialIcons';

import {
  WELCOME_SCREEN,
  LOGIN_SCREEN,
  HOME_SCREEN,
  FEED_SCREEN,
  NOTIFICATION_SCREEN,
  MORE_SCREEN,
  PROFILE_SCREEN,
} from './Screens';
import {SIDEMENU_COMPONENT} from './Components';
import registerScreens from './registerScreens';
import registerComponents from './registerComponents';
// Register all screens on launch
registerScreens();
registerComponents();

async function getMenuButton() {
  return {
    id: 'nav_button_menu',
    icon: await Icon.getImageSource('menu', 20, '#598BFF'),
    color: 'white',
  };
}

export function pushWelcomeScreenApp() {
  Navigation.setDefaultOptions({
    topBar: {
      background: {
        color: '#039893',
      },
      title: {
        color: 'white',
      },
      backButton: {
        title: '', // Remove previous screen name from back button
        color: 'white',
      },
      buttonColor: 'white',
    },
    statusBar: {
      style: 'light',
    },
    layout: {
      orientation: ['portrait'],
    },
    bottomTabs: {
      titleDisplayMode: 'alwaysShow',
    },
    bottomTab: {
      textColor: 'gray',
      selectedTextColor: 'black',
      iconColor: 'gray',
      selectedIconColor: 'black',
    },
  });

  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: WELCOME_SCREEN,
              options: {
                topBar: {
                  visible: false,
                },
                statusBar: {
                  style: 'dark',
                },
              },
            },
          },
        ],
      },
    },
  });
}

export function pushLoginScreenApp() {
  Navigation.setDefaultOptions({
    topBar: {
      background: {
        color: '#039893',
      },
      title: {
        color: 'white',
      },
      backButton: {
        title: '', // Remove previous screen name from back button
        color: 'white',
      },
      buttonColor: 'white',
    },
    statusBar: {
      style: 'light',
    },
    layout: {
      orientation: ['portrait'],
    },
    bottomTabs: {
      titleDisplayMode: 'alwaysShow',
    },
    bottomTab: {
      textColor: 'gray',
      selectedTextColor: 'black',
      iconColor: 'gray',
      selectedIconColor: 'black',
    },
  });

  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: LOGIN_SCREEN,
              options: {
                topBar: {
                  visible: false,
                },
                statusBar: {
                  style: 'dark',
                },
              },
            },
          },
        ],
      },
    },
  });
}

export async function pushProtectedScreenApp() {
  let TAB_MENU_BUTTON_INSTANCE = await getMenuButton();

  Navigation.setRoot({
    root: {
      sideMenu: {
        center: {
          bottomTabs: {
            id: 'bottom_tab_id',
            children: [
              {
                stack: {
                  id: `${HOME_SCREEN}_id`,
                  children: [

                    {
                      component: {
                        name: PROFILE_SCREEN,
                        options: {
                          topBar: {
                            title: {
                              text: 'Profile',
                            },
                          },
                        },
                      },
                    },
                    {
                      component: {
                        name: HOME_SCREEN,
                        options: {
                          topBar: {
                            title: {
                              text: 'Home',
                            },
                            leftButtons: [TAB_MENU_BUTTON_INSTANCE],
                          },
                        },
                      },
                    },
                  ],

                  options: {
                    bottomTab: {
                      icon: await Icon.getImageSource('home', 20, '#598BFF'),
                      testID: 'tab_home_button',
                      text: 'Home',
                    },
                  },
                },
              },
              {
                stack: {
                  id: `${FEED_SCREEN}_id`,
                  children: [
                    {
                      component: {
                        name: FEED_SCREEN,
                        options: {
                          topBar: {
                            title: {
                              text: 'Feeds',
                            },
                            leftButtons: [TAB_MENU_BUTTON_INSTANCE],
                          },
                        },
                      },
                    },
                  ],
                  options: {
                    bottomTab: {
                      icon: await Icon.getImageSource(
                        'rss-feed',
                        20,
                        '#598BFF',
                      ),
                      testID: 'tab_feed_button',
                      text: 'Feeds',
                    },
                  },
                },
              },
              {
                stack: {
                  id: `${NOTIFICATION_SCREEN}_id`,
                  children: [
                    {
                      component: {
                        name: NOTIFICATION_SCREEN,
                        options: {
                          topBar: {
                            title: {
                              text: 'Notification',
                            },
                            leftButtons: [TAB_MENU_BUTTON_INSTANCE],
                          },
                        },
                      },
                    },
                  ],
                  options: {
                    bottomTab: {
                      icon: await Icon.getImageSource(
                        'notifications',
                        20,
                        '#598BFF',
                      ),
                      testID: 'tab_notification_button',
                      text: 'Notifications',
                    },
                  },
                },
              },
              {
                stack: {
                  id: `${MORE_SCREEN}_id2`,
                  children: [
                    {
                      component: {
                        name: PROFILE_SCREEN,
                        options: {
                          topBar: {
                            title: {
                              text: 'Profile',
                            },
                          },
                        },
                      },
                    },
                    {
                      component: {
                        name: MORE_SCREEN,
                        options: {
                          topBar: {
                            title: {
                              text: 'More',
                            },
                            leftButtons: [TAB_MENU_BUTTON_INSTANCE],
                          },
                        },
                      },
                    },
                  ],
                  options: {
                    bottomTab: {
                      icon: await Icon.getImageSource(
                        'more-horiz',
                        20,
                        '#598BFF',
                      ),
                      testID: 'tab_more_button',
                      text: 'More',
                    },
                  },
                },
              },
            ],
          },
        },
        left: {
          component: {
            id: 'side_menu',
            name: SIDEMENU_COMPONENT,
          },
        },
      },
    },
  });
}
