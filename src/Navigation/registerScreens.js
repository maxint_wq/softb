import React from 'react';
import {Navigation} from 'react-native-navigation';

import {
  FeedScreen,
  HomeScreen,
  LandingScreen,
  LoginScreen,
  MoreScreen,
  NotificationScreen,
  
  ProfileScreen,
} from '../Screens';
import {default as Provider} from '../Store/provider';

import {
  WELCOME_SCREEN,
  LOGIN_SCREEN,
  HOME_SCREEN,
  FEED_SCREEN,
  NOTIFICATION_SCREEN,
  MORE_SCREEN,

  PROFILE_SCREEN
} from './Screens';

function WrappedComponent(Component) {
  return function inject(props) {
    const EnhancedComponent = () => (
      <Provider>
        <Component {...props} />
      </Provider>
    );
    return <EnhancedComponent />;
  };
}

export default function() {
  Navigation.registerComponent(WELCOME_SCREEN, () =>
    WrappedComponent(LandingScreen),
  );
  Navigation.registerComponent(LOGIN_SCREEN, () =>
    WrappedComponent(LoginScreen),
  );
  Navigation.registerComponent(HOME_SCREEN, () => WrappedComponent(HomeScreen));
  Navigation.registerComponent(FEED_SCREEN, () => WrappedComponent(FeedScreen));
  Navigation.registerComponent(NOTIFICATION_SCREEN, () =>
    WrappedComponent(NotificationScreen),
  );
  Navigation.registerComponent(MORE_SCREEN, () => WrappedComponent(MoreScreen));

  Navigation.registerComponent(PROFILE_SCREEN, () => WrappedComponent(ProfileScreen));
  console.info('All screens have been registered...');
}

