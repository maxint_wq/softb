export const WELCOME_SCREEN = 'softb.WelcomeScreen';
export const LOGIN_SCREEN = 'softb.LoginScreen';

export const HOME_SCREEN = 'softb.HomeScreen';
export const FEED_SCREEN = 'softb.FeedScreen';
export const NOTIFICATION_SCREEN = 'softb.NotificationScreen';
export const MORE_SCREEN = 'softb.MoreScreen';

export const PROFILE_SCREEN = 'softb.ProfileScreen';