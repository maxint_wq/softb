import React from 'react';
import {Navigation} from 'react-native-navigation';

import {SideMenuComponent} from '../Components';

import {default as Provider} from '../Store/provider';

import {SIDEMENU_COMPONENT, } from './Components';

function WrappedComponent(Component) {
  return function inject(props) {
    const EnhancedComponent = () => (
      <Provider>
        <Component {...props} />
      </Provider>
    );
    return <EnhancedComponent />;
  };
}

const CustomTopBarButton = () => {
  return <Text>TEST BUTTON</Text>;
};

export default function() {
  Navigation.registerComponent(SIDEMENU_COMPONENT, () =>WrappedComponent(SideMenuComponent),);

  console.info('All components have been registered...');
}
