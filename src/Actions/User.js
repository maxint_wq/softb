import http from '../Services/http';
import {pushLoginScreenApp, pushProtectedScreenApp} from "../Navigation";
export const LOGIN_USER = 'LOGIN_USER';
export const START_LOGIN_USER = 'START_LOGIN_USER';
export const ERROR_LOGIN_USER = 'ERROR_LOGIN_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const ADD_EMPLOYEE = 'ADD_EMPLOYEE';
export const DELETE_EMPLOYEE = 'DELETE_EMPLOYEE';
export const UPDATE_EMPLOYEE = 'UPDATE_EMPLOYEE';
export const LIST_EMPLOYEE = 'LIST_EMPLOYEE';

export const loginUser = data => ({
  type: 'LOGIN_USER',
  /* data = {userid:,userpassword} */
  data: data,
});

export const startloginUser = () => ({
  type: 'START_LOGIN_USER',
});

export const errorloginUser = data => ({
  /* data = string */
  type: 'ERROR_LOGIN_USER',
  data: data,
});


export const updateUser = data => ({
  /* data = {userid:,userpassword, name, department, cost_center, job_title}  or whatever editable field */
  type: 'UPDATE_USER',
  data: data,
});

export const updateEmployee = data => ({
  /* data = {userid:,userpassword, name, department, cost_center, job_title}  or whatever editable field */
  type: 'UPDATE_EMPLOYEE',
  data: data,
});

export const addEmployee = data => ({
  /* data = {userid:,userpassword, name, department, cost_center, job_title}  or whatever editable field */
  type: 'ADD_EMPLOYEE',
  data: data,
});

export const deleteEmployee = data => ({
  /* data = string field */
  type: 'DELETE_EMPLOYEE',
  data: data,
});

export const listEmployee = (data) =>({
  /* no filter support list all from endpoint*/
  type: 'LIST_EMPLOYEE',
  data: data
})

/* data = {userid:,userpassword} */
export const doLoginUser = data => {
  return async dispatch => {
    dispatch(startloginUser());
    try {
      let respond = await http.post('select * from test.user ');
      if (respond.status == false) {
        dispatch(errorloginUser('Error login'));
      } else {
        dispatch(
          loginUser({id: respond.data[0].id, userid: respond.data[0].userid, name: respond.data[0].name, department: respond.data[0].department, cost_center: respond.data[0].cost_center, job_title: respond.data[0].job_title}),
        );
        pushProtectedScreenApp()
      }
    } catch (e) {
      dispatch(errorloginUser('Error login'));
    }
  };
};

export const doUpdateUser = data => {
  return dispatch => {
    // update profile local
    // update remote http
    dispatch(updateUser(data));
  };
};

export const doUpdateEmployee = data => {
  console.log("update", data)
  return async dispatch => {
    let respond = await http.post(`update test.user set name='${data.name}', department='${data.department}', cost_center='${data.cost_center}', job_title='${data.job_title}' where id='${data.id}'  `);
    
    // update profile local
    // update remote http
    //dispatch(updateEmployee(data));
  };
};

export const doAddEmployee = data => {
  return async dispatch => {
    let respond = await http.post(`update test.user set name=${data.name}, department=${data.department}, cost_center=${data.cost_center}, job_title=${data.job_title} where id=${data.id}  `);
    console.log(respond)
    // http 
    // update profile
    //dispatch(addEmployee(data));
  };
};



export const doDeleteEmployee = data => {
  return dispatch => {
    // update http
    // update redux
    dispatch(deleteEmployee(data));
  };
};

export const doListEmployee = ()=>{
  return async dispatch =>{
    try {
      let respond = await http.post('select * from test.user ');
      if (respond.status == false) {
        dispatch(listEmployee({}))
      } else {
        console.log("doListEmployee",respond.data)
        dispatch(
          dispatch(listEmployee(respond.data))
        );
      }
    } catch (e) {
      dispatch(listEmployee({}))
    }

    
    
  }
}