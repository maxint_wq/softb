/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StyleSheet, TouchableOpacity, Text, View} from 'react-native';
import {Avatar, Layout} from '@ui-kitten/components';

const AvatarComponent: () => React$Node = ({
  user,
  onClickImage,
  onClickInfo,
  hidetitle,
  position,
}) => {
  function LoadAvatar() {
    let avatar = null;
    //console.log('user', user);
    if (user.profileimage == '') {
      avatar = require('../../assets/navigation/avatar.jpg');
    } else {
      avatar = {uri: 'data:image/jpeg;base64,' + user.profileimage};
    }
    return avatar;
  }

  return (
    <>
      <TouchableOpacity
        onPress={() => {
          onClickImage();
        }}>
        <View
          style={[styles.row, position == 'center' ? styles['center'] : '']}>
          <Avatar
            style={styles.avatar}
            shape="round"
            size="giant"
            source={LoadAvatar()}
          />
          {hidetitle == true ? <></> : <Text>{user.userid}</Text>}
        </View>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  center: {
    justifyContent: 'center',
  },
  avatar: {
    margin: 8,
  },
});

export default AvatarComponent;
