import {LOGIN_USER, START_LOGIN_USER, ERROR_LOGIN_USER, UPDATE_USER,
  LIST_EMPLOYEE, ADD_EMPLOYEE, DELETE_EMPLOYEE, UPDATE_EMPLOYEE
} from '../Actions/User';

export function User(
  state = {
    userprofile: {
      id: '',
      userid: '',
      profileimage: '',
      name: '',
      department: '',
      cost_center: '',
      job_title: ''
    },
    employeeIdx: {},
    loading: false,
    error: null,
  },
  action,
) {
  switch (action.type) {
    // personal profile
    case START_LOGIN_USER:
      return Object.assign({}, state, {
        loading: true,
        error: null,
      });
    case ERROR_LOGIN_USER:
      return Object.assign({}, state, {
        loading: false,
        error: action.data,
      });
    case LOGIN_USER:
      return Object.assign({}, state, {
        userprofile: {...state.userprofile,...action.data},
        loading: false
      });
    case UPDATE_USER:
      return Object.assign({}, state, {
        userprofile: {...state.userprofile,...action.data}
      });
    
    // employee / employee profile

    case LIST_EMPLOYEE: 
      let _employeeIdx = action.data.reduce(
        (acc, curr)=>{
          if(!acc[curr.id]){
            acc[curr.id] = curr;
          }
          return acc;
        },{}
      )
      return Object.assign(
        {}, state, {
          employeeIdx: {...state.employeeIdx, ... _employeeIdx}
        }
      )
    case ADD_EMPLOYEE:
      return Object.assign(
        {}, state, {
          employeeIdx: {...state.employeeIdx, ... action.data}
        }
      )
    case DELETE_EMPLOYEE:
      let _employee = state.employeeIdx;
      delete _employee[action.data];
      return Object.assign(
        {}, state, {
          employeeIdx: {..._employee}
        }
      )
    case UPDATE_EMPLOYEE:
      return Object.assign(
        {}, state, {
          employeeIdx: {..._employee}
        }
      )
    
    default:
      return state;
  }
}
