/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import {
  Button,
  Input,
  Icon,
  List,
  ListItem,
  withStyles,
} from '@ui-kitten/components';
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {Navigation} from 'react-native-navigation';
import {useState} from 'react';
import {doUpdateUser, doUpdateEmployee} from '../../Actions/User';
import {connect} from 'react-redux';
import {AvatarComponent} from '../../FunctionalComponents';

import ImagePicker from 'react-native-image-picker';

const options = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
class Profile extends React.Component {
  constructor(props) {
    super(props);
    console.log('props', props);
    this.state = {
      profile: this.props.id
        ? this.props.EmployeeId[this.props.id]
        : this.props.User.userprofile,
      name: '',
      department: '',
      cost_center: '',
      job_title: '',
    };
    this.navigationEventListener = Navigation.events().bindComponent(this);
  }
  componentWillUnmount() {
    if (this.navigationEventListener) {
      this.navigationEventListener.remove();
    }
  }
  navigationButtonPressed({buttonId}) {
    try {
      Navigation.mergeOptions('side_menu', {
        sideMenu: {
          left: {
            visible: true,
          },
        },
      });
    } catch (error) {
      //
    }
  }

  render() {
    const updateProfile = avatarSource => {
      this.props.dispatch(doUpdateUser({profileimage: avatarSource}));
    };

    const doUpdate = () => {
      this.props.dispatch(
        doUpdateEmployee({
          id: this.state.profile.id,
          name: this.state.profile.name,
          department: this.state.profile.department,
          cost_center: this.state.profile.cost_center,
          job_title: this.state.profile.job_title,
        }),
      );
    };

    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            <View style={styles.body}>
              <View style={styles.sectionContainer}>
                <View style={[styles.row]}>
                  <AvatarComponent
                    position="center"
                    user={this.state.profile}
                    hidetitle={true}
                    onClickImage={() => {}}
                    onClickInfo={() => {}}
                  />
                </View>
                <View style={[styles.row]}>
                  <Button
                    style={styles.button}
                    appearance="ghost"
                    onPress={async () => {
                      ImagePicker.showImagePicker(options, response => {
                        //console.log('Response = ', response);
                        if (response.didCancel) {
                          //console.log('User cancelled image picker');
                        } else if (response.error) {
                          //console.log('ImagePicker Error: ', response.error);
                        } else {
                          const source = {uri: response.uri};
                          // You can also display the image using data:
                          // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                          updateProfile(response.data);
                        }
                      });
                    }}>
                    Change Avatar
                  </Button>
                </View>
                <View styles={[styles.row]}>
                  <Input
                    placeholder="Name"
                    value={this.state.profile.name}
                    onChangeText={nextValue =>
                      this.setState({
                        profile: {...this.state.profile, ...{name: nextValue}},
                      })
                    }
                    textStyle={[]}
                    clearButtonMode="while-editing"
                    inlineImagePadding={5}
                  />
                </View>
                <View styles={[styles.row]}>
                  <Input
                    placeholder="Department"
                    value={this.state.profile.department}
                    onChangeText={nextValue =>
                      this.setState({
                        profile: {
                          ...this.state.profile,
                          ...{department: nextValue},
                        },
                      })
                    }
                    textStyle={[]}
                    clearButtonMode="while-editing"
                    inlineImagePadding={5}
                  />
                </View>
                <View styles={[styles.row]}>
                  <Input
                    placeholder="Cost  Center"
                    value={this.state.profile.cost_center}
                    onChangeText={nextValue =>
                      this.setState({
                        profile: {
                          ...this.state.profile,
                          ...{cost_center: nextValue},
                        },
                      })
                    }
                    textStyle={[]}
                    clearButtonMode="while-editing"
                    inlineImagePadding={5}
                  />
                </View>
                <View styles={[styles.row]}>
                  <Input
                    placeholder="Job Title"
                    value={this.state.profile.job_title}
                    onChangeText={nextValue =>
                      this.setState({
                        profile: {
                          ...this.state.profile,
                          ...{job_title: nextValue},
                        },
                      })
                    }
                    textStyle={[]}
                    clearButtonMode="while-editing"
                    inlineImagePadding={5}
                  />
                </View>
                <Button
                  style={[styles.actionButton]}
                  appearance="outline"
                  status="primary"
                  onPress={() => doUpdate()}>
                  Submit
                </Button>
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    display: 'flex',
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  button: {
    margin: 2,
  },
  row: {
    flex: 1,
  },
});

export const ThemedView = withStyles(Profile, theme => ({
  iconcolor: {
    backgroundColor: theme['color-basic-900'],
  },
}));

const mapStateToProps = state => {
  return {
    User: state.User,
    EmployeeId: state.User.employeeIdx,
  };
};

ThemedView.defaultProps = {
  User: {
    userprofile: {
      id: '',
      profileimage: '',
      userid: '',
      userpassword: '',
    },
  },
  EmployeeId: {},
  Profile: {},
};

export default connect(mapStateToProps)(ThemedView);
