/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {View, StatusBar} from 'react-native';
import {Input, Button, Spinner, Text} from '@ui-kitten/components';
import {withStyles} from '@ui-kitten/components';
import {StyleSheet} from 'react-native';

import {connect} from 'react-redux';
import {doLoginUser} from '../../Actions/User';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    margin: 10,
  },
  actionButton: {
    marginBottom: 10,
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const Login: () => React$Node = props => {
  const {eva, style, User, dispatch, ...restProps} = props;
  const [userid, setuserid] = React.useState('');
  const [userpassword, setuserpassword] = React.useState('');

  function doLogin() {
    dispatch(doLoginUser({id:123,userid: userid, userpassword: userpassword}));
  }

  const LoadingIndicator = props => (
    <View style={[props.style, styles.indicator]}>
      <Spinner size="small" />
    </View>
  );

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <View style={[styles.container]}>
        <Input
          placeholder="User ID"
          value={userid}
          onChangeText={nextValue => setuserid(nextValue)}
          textStyle={[]}
          autoCompleteType="email"
          clearButtonMode="while-editing"
          importantForAutofill="yes"
          inlineImageLeft="search_icon"
          inlineImagePadding={5}
          keyboardType="email-address"
          returnKeyLabel="Next"
          returnKeyType="next"
        />
        <Input
          placeholder="User Password"
          value={userpassword}
          onChangeText={nextValue => setuserpassword(nextValue)}
          secureTextEntry={true}
          autoCompleteType="password"
          clearButtonMode="while-editing"
          clearTextOnFocus={true}
          importantForAutofill="yes"
          inlineImageLeft="search_icon"
          inlineImagePadding={5}
          maxLength={10}
          returnKeyLabel="Login"
          returnKeyType="done"
        />
        <Button
          style={[styles.actionButton]}
          appearance="outline"
          status="primary"
          accessoryLeft={User.loading == true ? LoadingIndicator : ''}
          disabled={User.loading == true ? true : false}
          onPress={() => doLogin()}>
          Login
        </Button>
        <Button
          style={[styles.actionButton]}
          appearance="outline"
          status="basic"
          accessoryLeft={User.loading == true ? LoadingIndicator : ''}
          disabled={User.loading == true ? true : false}
          onPress={() => console.log('Forgot Password Pressed')}>
          Forgot Password
        </Button>

        <Text category="s1" status="danger">
          {User.error}
        </Text>
      </View>
    </>
  );
};

export const ThemedView = withStyles(Login, theme => ({}));

const mapStateToProps = state => ({
  User: state.User,
});

ThemedView.defaultProps = {};

export default connect(mapStateToProps)(ThemedView);
