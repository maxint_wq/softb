/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StyleSheet, View} from 'react-native';
import {ActivityIndicator} from 'react-native';
import { connect } from 'react-redux';
import {pushLoginScreenApp, pushProtectedScreenApp} from "../../Navigation";

const Landing: () => React$Node = ({User}) => {

  React.useEffect(()=>{
    if(User.userprofile.id==''){
      pushLoginScreenApp();
    }else{
      pushProtectedScreenApp();
    }
  }, [])

  return (
    <>
      <View style={[styles.container]}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
});

const mapStateToProps = (state) => ({
  User: state.User,
});

Landing.defaultProps={}

export default connect(mapStateToProps)(Landing);
