/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
} from 'react-native';

import {withStyles} from '@ui-kitten/components';

import {connect} from 'react-redux';

import {doListEmployee} from '../../Actions/User';

import {MORE_SCREEN, PROFILE_SCREEN} from '../../Navigation/Screens';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {Navigation} from 'react-native-navigation';
import {useState} from 'react';

class More extends React.Component {
  constructor(props) {
    super(props);
    this.navigationEventListener = Navigation.events().bindComponent(this);
  }
  componentWillUnmount() {
    if (this.navigationEventListener) {
      this.navigationEventListener.remove();
    }
    this.props.dispatch(doListEmployee());
  }
  navigationButtonPressed({buttonId}) {
    try {
      Navigation.mergeOptions('side_menu', {
        sideMenu: {
          left: {
            visible: true,
          },
        },
      });
    } catch (error) {
      //
    }
  }

  render() {
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
        
            <View style={styles.body}>
              <View style={styles.sectionContainer}>
                <Text style={styles.sectionTitle}>Employee</Text>
              </View>
  
              <FlatList
                data={this.props.EmployeeList}
                renderItem={({item}) => (
                  <Text style={styles.item} onPress={()=>{
                    //console.log(this.props.EmployeeIdx[item])
                    Navigation.push(`${MORE_SCREEN}_id`, {
                      component: {
                        passProps: {
                          id: this.props.EmployeeIdx[item].id,
                        },
                        name: PROFILE_SCREEN,
                        options: {
                          sideMenu: {
                            left: {
                              visible: false,
                            },
                          },
                        },
                      },
                    });
                  }}>{this.props.EmployeeIdx[item].userid} - {this.props.EmployeeIdx[item].job_title}</Text>
                )}
              />
            </View>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export const ThemedView = withStyles(More, theme => ({}));

const mapStateToProps = state => ({
  User: state.User,
  EmployeeIdx: state.User.employeeIdx,
  EmployeeList: Object.keys(state.User.employeeIdx)
});

ThemedView.defaultProps = {};

export default connect(mapStateToProps)(ThemedView);
