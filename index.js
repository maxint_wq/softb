/**
 * @format
 */

import { Navigation } from 'react-native-navigation';
import { pushWelcomeScreenApp } from './src/Navigation';

Navigation.events().registerAppLaunchedListener(() => pushWelcomeScreenApp());
